# Chroxy

Chroxy run chrome headless and a proxy service to manage the chrome processes.

Chrome + Proxy = Chroxy

## Usage

Import the image into lxd.

``` bash
lxc image import chroxy.tar.gz --alias chroxy
lxc launch chroxy chroxy-1
```

Run this command to launch chroxy in development

``` bash
lxc exec chroxy-1 -- sudo --login --user ubuntu bash -c 'PATH=/home/ubuntu/.asdf/shims:$PATH mix run --no-halt'
```